<?php

/**
 * Chess Board.
 *
 * By convention we assume the white pieces are placed on the bottom of the
 * board and that the coordinate (0,0) is the bottom left corner of the board.
 *
 * We're using a zero based array for the board but chess boards are typically
 * notated with a one based rank.  Files are labelled from a to h
 *
 * White pawns therefore move from rank 2 to rank 8 and black pawns advance
 * from rank 7 to rank 1
 *
 * PHP version 5.4+
 *
 * @author     Andy Beak <andy.beak@double-eye.com>
 * @copyright  2015 Logic Now
 * @license    Commercial - All rights reserved
 *
 * @version    1.0.0
 *
 * @since      File available since Release 1.0.0
 */

namespace LogicNow;

use Exception;
use Pieces\BasePiece;
use Moves\MovementTypeEnum;
use Pieces\PieceColorEnum;

class ChessBoard
{
    const MAX_BOARD_WIDTH = 8;
    const MAX_BOARD_HEIGHT = 8;

    const MIN_BOARD_WIDTH = 1;
    const MIN_BOARD_HEIGHT = 1;

    private $_pieces;

    /**
     * When we remove a piece from the board we place it in the graveyard so
     * that if we reverse the move we can get the piece back in the state it
     * was in (i.e.: with its flags about moving preserved)
     *
     * @var $graveyard
     * @type array
     */
    private $graveyard = [];

    private static $instance = false;

    public function __construct()
    {
        $this->_pieces = array_fill(self::MIN_BOARD_WIDTH, self::MAX_BOARD_WIDTH,
            array_fill(self::MIN_BOARD_HEIGHT, self::MAX_BOARD_HEIGHT, 0));

        self::$instance = $this;
    }

    /**
     * addPiece.
     *
     * Add a piece to the board.
     * Return (-1,-1) if the piece could not be added
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param Pieces\Piece $piece        Note call by reference
     * @param int          $_xCoordinate
     * @param int          $_yCoordinate
     */
    public function addPiece(BasePiece &$piece, $_xCoordinate, $_yCoordinate)
    {
        try {
            if (!$this->isLegalBoardPosition($_xCoordinate, $_yCoordinate)) {
                throw new Exception('Cannot add piece to '.
                    'that position because it is not valid');
            }

            if (!empty($this->_pieces[$_xCoordinate][$_yCoordinate])) {
                throw new Exception('There is already a piece in that position');
            }

            $currentCount = $this->countPieceType($piece->getPieceNotationSymbol(), $piece->getPieceColor());

            $maxCount = $piece->getMaxAllowed();

            if (++$currentCount > $maxCount) {
                throw new Exception('You may only place '.$maxCount.' of '.
                    ' those pieces on the board');
            }

            $piece->setXCoordinate($_xCoordinate);

            $piece->setYCoordinate($_yCoordinate);

            $this->_pieces[$_xCoordinate][$_yCoordinate] = $piece;
        } catch (Exception $e) {
            $piece->setXCoordinate(-1);

            $piece->setYCoordinate(-1);

            // Rethrow the exception so that our game controller can catch
            // it and display a user friendly message.  This would fail the
            // tests though.

            // throw new \Exceptions\PositionException($e->getMessage());
        }
    }

    /**
     * countPieces.
     *
     * Returns an array of the count of pieces on the board.  We can use this
     * when adding pieces or checking for stalemate.
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param string $notationSymbol
     * @param Piece
     *
     * @return array
     */
    private function countPieces()
    {
        $pieces = [
            PieceColorEnum::WHITE => [],
            PieceColorEnum::BLACK => [],
        ];

        foreach ($this->_pieces as $row => $column) {
            foreach ($column as $key => $piece) {
                if (empty($piece)) {
                    continue;
                }

                $colour = $piece->getPieceColor();

                $notationSymbol = $piece->getPieceNotationSymbol();

                if (!isset($pieces[$colour][$notationSymbol])) {
                    $pieces[$colour][$notationSymbol] = 0;
                }

                $pieces[$colour][$notationSymbol]++;
            }
        }

        return $pieces;
    }

    /**
     * countPieceType.
     *
     * Returns a count of the pieces on the board with the given notation
     * symbol for the colour specified.
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param string $notationSymbol
     * @param int    $colour
     *
     * @return int
     */
    private function countPieceType($notationSymbol, $colour)
    {
        $pieces = $this->countPieces();

        if (!isset($pieces[$colour][$notationSymbol])) {
            return 0;
        } else {
            return $pieces[$colour][$notationSymbol];
        }
    }

    /**
     * getMoveType.
     *
     * We want to know if a move is a standard move or a capture.  An exception
     * is thrown if the move is invalid, which should be caught and handled in
     * the game object
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $fromX
     * @param int $fromY
     * @param int $toX
     * @param int $toY
     *
     * @return Moves\MovementTypeEnum
     * @throws \Exceptions\MoveException
     */
    public function getMoveType($fromX, $fromY, $toX, $toY)
    {
        $fromPiece = $this->getPieceAt($fromX, $fromY);

        if (!$this->isSquareEmpty($toX, $toY))
        {
            $toPiece = $this->getPieceAt($toX, $toY);

            if($fromPiece->getPieceColor() == $toPiece->getPieceColor()) {
                throw new \Exceptions\MoveException('You cannot move to an occupied square');
            }

            $moveType = MovementTypeEnum::CAPTURE();

        } else {

            $moveType = MovementTypeEnum::MOVE();

        }

        return $moveType;
    }

    /**
     * getStandardNotation.
     *
     * Returns the standard notation for the coordinate.
     * The bottom left square is a1 and the top right is h8
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $x
     * @param int $y
     *
     * @return mixed
     */
    public static function getStandardNotation($x, $y)
    {
        $leftCol = self::MIN_BOARD_WIDTH;

        $x -= $leftCol;

        $square = chr(ord('a') + $x) . $y;

        return $square;
    }

    /**
     * getPieceAt.
     *
     * Returns the piece on the board at x,y (or 0 if there is none)
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $x
     * @param int $y
     *
     * @return mixed
     */
    public function getPieceAt($x, $y)
    {
        return $this->_pieces[$x][$y];
    }


    /**
     * isLegalBoardPosition.
     *
     * Check that the supplied coordinates fall within the 8x8 grid of a
     * chessboard.
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $_xCoordinate
     * @param int $_yCoordinate
     *
     * @return bool
     */
    public function isLegalBoardPosition($_xCoordinate, $_yCoordinate)
    {
        // validate that x is integer and in the range 0 to 7
        $xValid = filter_var($_xCoordinate,
            FILTER_VALIDATE_INT,
            [
                'options' => [
                    'min_range' => self::MIN_BOARD_WIDTH,
                    'max_range' => self::MAX_BOARD_WIDTH,
                ],
            ]
        );

        // validate that y is integer and in the range 0 to 7
        $yValid = filter_var($_yCoordinate,
            FILTER_VALIDATE_INT,
            [
                'options' => [
                    'min_range' => self::MIN_BOARD_WIDTH,
                    'max_range' => self::MAX_BOARD_HEIGHT,
                ],
            ]
        );

        return ($xValid !== false) && ($yValid !== false);
    }

    /**
     * isSquareEmpty.
     *
     * Returns boolean depending on whether the square specified in
     * the parameters is empty.  True = square is empty and has no piece
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $x
     * @param int $y
     *
     * @return bool
     */
    public function isSquareEmpty($x, $y)
    {
        return(empty($this->_pieces[$x][$y]));
    }

    /**
     * movePiece.
     *
     * Move a piece from one position on the board to another
     * No validation is performed if this is a valid move - this
     * method is called from a Piece which is responsible for checking
     * validity.  We specify the fromX and fromY so that we don't create
     * a dependency on the Piece and seperate the concern of moving (consider
     * the alternative of using a parameter for the piece being moved)
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $fromX
     * @param int $fromY
     * @param int $toX
     * @param int $toY
     *
     * @throws \Exceptions\MoveException
     */
    public function movePiece($fromX, $fromY, $toX, $toY)
    {
        if (!$this->isLegalBoardPosition($fromX, $fromY) ||
            !$this->isLegalBoardPosition($toX, $toY)) {
            throw new \Exceptions\MoveException('That is not a valid board
                position');
        }

        $piece = $this->_pieces[$fromX][$fromY];

        if (!is_subclass_of($piece, 'Pieces\BasePiece')) {
            throw new \Exceptions\MoveException('There is no piece at that
                position to move');
        }

        $this->_pieces[$toX][$toY] = $piece;

        $piece->setXCoordinate($toX);

        $piece->setYCoordinate($toY);

        $this->_pieces[$toX][$toY] = 0;
    }

    /**
     * movePiece.
     *
     * Remove a piece from the board and place it in the graveyard where
     * it can be restored if the player reverses their move.  We do this
     * so that we preserve information about whether it has moved or not
     * so that special moves (en passant, moving twice, and castling) are
     * still allowed or disallowed as the case may be.
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $x
     * @param int $y
     *
     * @throws \Exceptions\MoveException
     */
    public function removePiece($x, $y)
    {
    	$piece = $this->getPieceAt($x, $y);

    	$graveyard[] = $piece;

    	$this->_pieces[$x][$y] = 0;
    }
}
