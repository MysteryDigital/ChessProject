<?php

namespace Exceptions;

/**
 * MoveException Class.
 *
 * Used to indicate that a selected move is invalid.  Should be caught in
 * the game controller logic so that a user friendly error message can
 * be displayed.
 */
class MoveException extends \Exception
{
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
