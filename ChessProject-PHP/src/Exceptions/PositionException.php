<?php

namespace Exceptions;

/**
 * PositionException Class.
 *
 * Used to indicate that a position being referenced on the board is invalid.
 * Should be caught in play controller logic and handled appropriately.
 */
class PositionException extends \Exception
{
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
