<?php

/**
 * Game.php.
 *
 * PHP version 5.4+
 *
 * @author     Andy Beak <andy.beak@protonmail.ch>
 * @copyright  2015 Logic Now
 * @license    Commercial - All rights reserved
 *
 * @version    1.0.0
 *
 * @since      File available since Release 1.0.0
 */

namespace LogicNow;

use LogicNow\Chessboard;
use Moves\MoveList;
use Pieces\PieceFactory;

/**
 * Game class - One class to rule them all.  The game class
 * is responsible for directing the game logic and returning
 * json encoded responses to our frontend.
 *
 * @author      Andy Beak <andy.beak@protonmail.ch>
 * @copyright   2015 Logic Now
 * @license     Commercial
 *
 * @since       Class available since Release 1.0.0
 */
class Game
{

	/**
	 * Master instance of the chessboard
	 *
	 * @var $chessboard
	 * @type LogicNow\Chessboard
	 */
	private $chessboard;

	/**
	 * Master instance of the move list
	 *
	 * @var $movelist;
	 * @type Moves\MoveList
	 */
	private $movelist;

	public function __construct()
	{
		$this->chessBoard = new Chessboard();

		$this->movelist = new MoveList();
	}

    /**
     * move.
     *
     * Implement logic for moving pieces.  This function is called from the
     * frontend by an XHR post.
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $_xCoordinate
     * @param int $_yCoordinate
     *
     * @return bool
     */
	public function move()
	{

		try
		{
			// get variables from $_POST
			$postVarsWhitelist = ['fromX', 'fromY', 'toX', 'toY', 'playerNumber'];

			$postVars = [];

			foreach($postVarsWhitelist as $key) {
				if(!isset($_POST[$key])) {
					throw new \Exceptions\MoveException('Missing move information');
				}
				$postVars[] = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
			}

			list($fromX, $fromY, $toX, $toY, $playerNumber) = $postVars;	// we know it's in the right order

			// determine move type
			$moveType = $this->chessBoard->getMoveType($fromX, $fromY, $toX, $toY);

			// move piece (the piece updates the board in the original design - should we refactor this to seperate concerns?)
			$piece = $this->chessBoard->getPieceAt($fromX, $fromY);

			$piece->move($moveType, $toX, $toY);

			// add to move list
			$this->movelist->addMove(
				$moveType,
         		$piece,
         		$toX,
         		$toY,
         		$playerNumber,
         		// TODO: pawn promotion
			);

		} catch (\Exceptions\MoveException $e) {
			$message = $e->getMessage();
			$success = false;
		}

		$returnArray = compact('success', 'message');

		header('Content-Type: application/json');

		echo json_encode($returnArray);
	}


	public function setUpStandardBoard()
	{
		// add pawns
		for($i = MIN_BOARD_WIDTH; $i<=Chessboard::MAX_BOARD_WIDTH; $i++)
		{
			$whitePawn = PieceFactory::make('pawn', PieceColorEnum::WHITE);
			$whitePawn->setChessboard($this->chessboard);
			$blackPawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);
			$blackPawn->setChessboard($this->chessboard);

			$this->chessboard->addPiece($whitePawn, $i, 2);
			$this->chessboard->addPiece($blackPawn, $i, 7);
		}

		// TODO: add other pieces
	}

}