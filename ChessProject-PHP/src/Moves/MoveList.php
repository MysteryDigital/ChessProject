<?php

/**
 * MoveList.php.
 *
 * PHP version 5.4+
 *
 * @author     Andy Beak <andy.beak@protonmail.ch>
 * @copyright  2015 Logic Now
 * @license    Commercial - All rights reserved
 *
 * @version    1.0.0
 *
 * @since      File available since Release 1.0.0
 */

namespace Moves;

use Pieces\BasePiece;
use LogicNow\Player;
use Moves\MovementTypeEnum;

/**
 * MoveList class - used to contain a list of the moves in the game.
 *
 * @author      Andy Beak <andy.beak@protonmail.ch>
 * @copyright   2015 Logic Now
 * @license     Commercial
 *
 * @since       Class available since Release 1.0.0
 */
class MoveList
{
    private $moveNum = 0;
    private $players = [];
    private $movelist = [];

    /**
     * Creates a new MoveList.
     *
     * @param LogicNow\Player $playerOne
     * @param LogicNow\Player $playerTwo
     *
     * @return MoveList
     */
    public function __construct()
    {
    }

     /**
      * Adds a new move to the list of moves.
      *
      * @param Moves\MovementTypeEnum $movementTypeEnum
      * @param Pieces\BasePiece $piece
      * @param int $newX
      * @param int $newY
      * @param string $promotedTo optional
      *
      * @return MoveList
      */
     public function addMove(MovementTypeEnum $movementTypeEnum,
         BasePiece $piece, $toX, $toY, $playerNumber, $promotedTo = null)
     {
         $currentMove = ++$this->moveNum;

         $fromX = $piece->getXCoordinate();

         $fromY = $piece->getYCoordinate();

         $symbol = $movementTypeEnum == MovementTypeEnum::CAPTURE() ? 'x' : '-';

         $pieceName = $piece->toString();

         $this->movelist[$currentMove] = new MoveListItem($currentMove, $playerNumber,
         	$pieceName, $fromX, $fromY, $toX, $toY, $symbol, null);

     }

	/**
	* getMove.
	*
	* Returns the move identified by moveNum
	*
	* @return array
	*/
	public function getMove($moveNum)
	{
		$moveNum = filter_var($moveNum, FILTER_SANITIZE_NUMBER_INT);
		$moveNum = max($this->moveNum, $moveNum);
		return $this->movelist[$moveNum];
	}

	/**
	* getMoveList.
	*
	* Returns the array of historical moves
	*
	* @return array
	*/
	public function getMoveList()
	{
		return $this->movelist;
	}
	/**
	* getMoveNumber.
	*
	* Returns the notation of a historical move
	*
	* @return int
	*/

	public function getMoveNotation($moveNum)
	{
		$move = $this->getMove($moveNum);

		return $move->getNotationString();
	}

	/**
	* getMoveNumber.
	*
	* Returns the current move number
	*
	* @return int
	*/

	public function getMoveNum()
	{
		return (int)$this->moveNum;
	}


}
