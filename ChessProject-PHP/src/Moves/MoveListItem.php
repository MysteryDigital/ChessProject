<?php

/**
 * MoveListItem.php.
 *
 * PHP version 5.4+
 *
 * @author     Andy Beak <andy.beak@protonmail.ch>
 * @copyright  2015 Logic Now
 * @license    Commercial - All rights reserved
 *
 * @version    1.0.0
 *
 * @since      File available since Release 1.0.0
 */

namespace Moves;

use LogicNow\Chessboard;

/**
 * MoveListItem class - an entry in the move list.
 * We want to be able to enumerate through these items
 * for purposes of displaying a list of moves.
 *
 * @author      Andy Beak <andy.beak@protonmail.ch>
 * @copyright   2015 Logic Now
 * @license     Commercial
 *
 * @since       Class available since Release 1.0.0
 */
class MoveListItem
{
    /**
     * @var int
     */
    private $moveNum = 0;

    /**
     * @var string
     */
    private $pieceName = 0;

    /**
     * @var int
     */
    private $fromX;

    /**
     * @var int
     */
    private $fromY;

    /**
     * @var int
     */
    private $toX;

    /**
     * @var int
     */
    private $toY;

    /**
     * Symbol to use when displaying in chess notation
     * a hyphen - indicates a normal move
     * a letter x indicates a capture.
     *
     * @var string
     */
    private $moveSymbol;

    /**
     * @var string
     */
    private $promotedTo = null;

    public function __construct($moveNumber, $playerNumber, $pieceName,
        $fromX, $fromY, $toX, $toY, $symbol, $promotedTo)
    {

        // We filter the variables because we might display them in a list and
        // we want to be 100% certain that we're not storing an XSS attack somehow
        $this->moveNum = filter_var($moveNumber, FILTER_SANITIZE_NUMBER_INT);

        $this->pieceName = filter_var($pieceName, FILTER_SANITIZE_STRING);

        $this->fromX = filter_var($fromX, FILTER_SANITIZE_NUMBER_INT);

        $this->fromY = filter_var($fromY, FILTER_SANITIZE_NUMBER_INT);

        $this->toX = filter_var($toX, FILTER_SANITIZE_NUMBER_INT);

        $this->toY = filter_var($toY, FILTER_SANITIZE_NUMBER_INT);

        $this->moveSymbol = filter_var($symbol, FILTER_SANITIZE_STRING);

        $this->promotedTo = filter_var($promotedTo, FILTER_SANITIZE_STRING);
    }

    public function getFromX()
    {
    	return $this->fromX;
    }

    public function getFromY()
    {
    	return $this->fromY;
    }

    public function getToX()
    {
    	return $this->toX;
    }

    public function getToY()
    {
    	return $this->toY;
    }

    public function getPieceName()
    {
    	return $this->pieceName;
    }

    public function getNotationString()
    {
    	$fromSquare = Chessboard::getStandardNotation($this->fromX, $this->fromY);

    	$toSquare = Chessboard::getStandardNotation($this->toX, $this->toY);

    	return $this->pieceName . $fromSquare . $this->moveSymbol . $toSquare;
    }

}
