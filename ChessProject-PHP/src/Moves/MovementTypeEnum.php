<?php

namespace Moves;

class MovementTypeEnum
{
    private static $_instance = false;
    private static $_move;
    private static $_capture;
    private static $_enPassant;

    private $_id;

    private function __construct($_id)
    {
        $this->_id = $_id;
    }

    /** @return: MovementTypeEnum */
    public static function MOVE()
    {
        self::initialise();

        return self::$_move;
    }

    /** @return: MovementTypeEnum */
    public static function CAPTURE()
    {
        self::initialise();

        return self::$_capture;
    }

    /** @return: MovementTypeEnum */
    public static function EN_PASSANT()
    {
        self::initialise();

        return self::$_enPassant;
    }

    private static function initialise()
    {
        if (self::$_instance) {
            return;
        }

        self::$_move = new MovementTypeEnum(1);
        self::$_capture = new MovementTypeEnum(2);
        self::$_enPassant = new MovementTypeEnum(3);
    }
}
