<?php

/**
 * BasePiece.php.
 *
 * PHP version 5.4+
 *
 * @author     Andy Beak <andy.beak@protonmail.ch>
 * @copyright  2015 Logic Now
 * @license    Commercial - All rights reserved
 *
 * @version    1.0.0
 *
 * @since      File available since Release 1.0.0
 */

namespace Pieces;

use Exception;
use Moves\MovementTypeEnum;
use LogicNow\Chessboard;
use Moves\MoveList;

/**
 * BasePiece class - an abstract class used to provide inheritance common
 * to all the pieces.
 *
 * @author      Andy Beak <andy.beak@protonmail.ch>
 * @copyright   2015 Logic Now
 * @license     Commercial
 *
 * @since       Class available since Release 1.0.0
 */
abstract class BasePiece
{
    /**
     *@var Integer
     */
    private $_pieceColor;
    /** @var  ChessBoard */
    private $_chessBoard;
    /** @var  int */
    private $_xCoordinate;
    /** @var  int */
    private $_yCoordinate;

    /**
     * Has this Piece been moved before?  Pawns may advance two spaces on their
     * first turn.  You may not castle if the king or castle has moved before.
     *
     * @var bool
     */
    protected $hasMoved = false;

    /**
     * How many of this sort of piece each player may have.
     *
     * @var int
     */
    private $maxAllowed;

    /**
     * The symbol used to represent the piece in move lists.  We cannot use
     * the first letter of the piece name because we may want to use R for
     * Rooks (Castles) or N for Knights.
     *
     * @var string
     */
    private $pieceNotationSymbol;

    public function __construct($pieceColor)
    {
        $this->_pieceColor = $pieceColor;

        $this->setMaxAllowed = 2;    // a default

        $this->hasMoved = false;
    }

    /**
     * checkValidMove.
     *
     * Contract for the piece to fulfil.  It must be able to check if
     * a specified move is valid.
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param Moves\MovementTypeEnum $movementType
     * @param int                       $_xCoordinate
     * @param int                       $_yCoordinate
     *
     * @return bool
     */
    abstract protected function checkValidMove(
        MovementTypeEnum $movementType,
        $_xCoordinate,
        $_yCoordinate
    );

    public function getChesssBoard()
    {
        return $this->_chessBoard;
    }

    /**
     * setChessBoard.
     *
     * Dependency injection of the chessboard
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param LogicNow\Chessboard $chessBoard
     *
     * @return bool
     */
    public function setChessBoard(Chessboard $chessBoard)
    {
    	$this->_chessBoard = $chessBoard;
    }

    /** @return int */
    public function getXCoordinate()
    {
        return $this->_xCoordinate;
    }

    /** @var int */
    public function setXCoordinate($value)
    {
        $this->_xCoordinate = $value;
    }

    /** @return int */
    public function getYCoordinate()
    {
        return $this->_yCoordinate;
    }

    /** @var int */
    public function setYCoordinate($value)
    {
        $this->_yCoordinate = $value;
    }

    public function getPieceColor()
    {
        return $this->_pieceColor;
    }

    public function getPieceNotationSymbol()
    {
        return $this->pieceNotationSymbol;
    }

    public function getMaxAllowed()
    {
        return $this->maxAllowed;
    }

    public function setMaxAllowed($amount)
    {
        $amount = max(ChessBoard::MAX_BOARD_WIDTH,
            filter_var($amount, FILTER_SANITIZE_NUMBER_INT));

        $this->maxAllowed = $amount;
    }

    /**
     * setPieceNotationSymbol.
     *
     * The setter is access protected so that only the piece (which is the only
     * child of this class) can set the piece name.  We don't want the board
     * or the player objects (for example) to rename pieces.
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access protected
     *
     * @param int $_xCoordinate
     * @param int $_yCoordinate
     *
     * @return bool
     */
    protected function setPieceNotationSymbol($name)
    {
        $this->pieceNotationSymbol = filter_var($name, FILTER_SANITIZE_STRING);
    }

    /**
     * move.
     *
     * Implement logic for the pawn to move
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $_xCoordinate
     * @param int $_yCoordinate
     *
     * @return bool
     */
    public function move(MovementTypeEnum $movementTypeEnum, $newX, $newY)
    {
        try {

            // invoke piece specific check for valid move
            $this->checkValidMove($movementTypeEnum, $newX, $newY);

            $fromX = $this->getXCoordinate();

            $fromY = $this->getYCoordinate();

            $this->_chessBoard->movePiece($fromX, $fromY, $newX, $newY);

            $this->setXCoordinate($newX);

            $this->setYCoordinate($newY);

            $this->hasMoved = true;

        } catch (Exception $e) {
            // TODO: we should throw a user friendly message so that our
            // game controller can display it, but this breaks the unit
            // tests
            // throw new \Exceptions\MoveException($e->getMessage);

        }
    }

    public function setPieceColor($value)
    {
        $this->_pieceColor = $value;
    }

    public function toString()
    {
        return $this->getPieceNotationSymbol();
    }

    protected function currentPositionAsString()
    {
        $result = 'Current X: '.$this->_xCoordinate.PHP_EOL;
        $result .= 'Current Y: '.$this->_yCoordinate.PHP_EOL;
        $result .= 'Piece Color: '.PieceColorEnum::getColor($this->_pieceColor);

        return $result;
    }
}
