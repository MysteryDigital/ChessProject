<?php

/**
 * Pawn.php.
 *
 * PHP version 5.4+
 *
 * @author     Andy Beak <andy.beak@protonmail.ch>
 * @copyright  2015 Logic Now
 * @license    Commercial - All rights reserved
 *
 * @version    1.0.0
 *
 * @since      File available since Release 1.0.0
 */

namespace Pieces;

use Exceptions;
use LogicNow\Chessboard;
use Moves\MovementTypeEnum;

/**
 * Pawn class extends the base piece class and implements properties
 * specific to pawns.
 *
 * @author      Andy Beak <andy.beak@protonmail.ch>
 * @copyright   2015 Logic Now
 * @license     Commercial
 *
 * @since       Class available since Release 1.0.0
 */
class Pawn extends BasePiece
{
    public function __construct($pieceColor)
    {
        parent::__construct($pieceColor);

        $this->setPieceNotationSymbol('P');

        $this->setMaxAllowed(ChessBoard::MAX_BOARD_WIDTH + 1);
    }

 	 /**
     * checkMovingForward.
     *
     * Make sure that a pawn is moving forward or not
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $newX the target X position
     * @param int $newY the target Y position
     *
     * @return bool;
     * @throws new Exceptions\MoveException
     */
    private function checkMovingForward($toY)
    {
    	$currentY = $this->getYCoordinate();

    	$movingFowards = true;

    	if ($this->getPieceColor() == PieceColorEnum::BLACK) {
	    	if ($toY >= $currentY) {
	    		$movingFowards = false;
	        }
    	} else {
    		if ($toY <= $currentY) {
    			$movingFowards = false;
	        }
    	}

    	if (!$movingFowards) {
    		throw new Exceptions\MoveException('Pawns may only advance away from their king');
    	}

    	return true;
    }

    /**
     * checkValidMove.
     *
     * Implement logic to test if a pawn moving to a position is valid or not
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param int $newX the target X position
     * @param int $newY the target Y position
     *
     * @return bool
     */
    protected function checkValidMove(
        MovementTypeEnum $movementType,
        $toX,
        $toY
    ) {
        switch ($movementType) {
            case MovementTypeEnum::MOVE() :

                // test Y ranks
                $squaresToMove = 1;

                if (false == $this->hasMoved) {
                    $squaresToMove++;
                }

                $currentY = $this->getYCoordinate();

                $validMove = true;

                if ($this->getPieceColor() == PieceColorEnum::BLACK) {
                    $maxEndingY = $currentY - $squaresToMove;

                    if ($maxEndingY > $toY) {
                        throw new Exceptions\MoveException('Pawns may only advance one square at a time, ' .
                        	'unless it is their first move in which case they may move two');
                    }

                    $this->checkMovingForward($toY);

                } else {
                    $maxEndingY = $currentY + $squaresToMove;
                    if ($maxEndingY < $toY) {
                        throw new Exceptions\MoveException('Pawns may only advance one square at a time, ' .
                        	'unless it is their first move in which case they may move two');
                    }

                    $this->checkMovingForward($toY);
                }

                // if not capturing pawns may only advance in the same file
                $currentX = $this->getXCoordinate();

                if ($currentX != $toX) {
                    throw new Exceptions\MoveException('Pawns may only advance in the same file');
                }

                break;
            case MovementTypeEnum::CAPTURE() :

                $this->checkMovingForward($toY);

                $currentX = $this->getXCoordinate();

                if(abs($toX - $currentX) !== 1) {
                	throw new Exceptions\MoveException('Pawns capture by moving one space diagonally forwards, ' .
                		'unless they are performing en passant');
                }

                // this method is called after Chessboard::getMoveType has already
                // established a piece of the opposite colour is in the square.
                // We would otherwise test that here.

                break;
            case MovementTypeEnum::EN_PASSANT() :
                throw new \Exception('Must implement en passant');
                break;
            default :
                throw new \Exceptions\MoveException('Pawns may not perform '.
                    'that type of move.');
        }
    }

    /**
     * promote.
     *
     * A pawn advanced to the final rank must be promoted to a piece
     * of any type other than King (may not remain pawn)
     *
     * @version 1.0.0
     *
     * @author Andy Beak
     *
     * @since 1.0.0
     * @access public
     *
     * @param Pieces\Piece          $piece
     * @param int                   $_xCoordinate
     * @param int                   $_yCoordinate
     * @param Pieces\PieceColorEnum $pieceColor
     *
     * @return array
     */
    public function promote(BasePiece $newPiece)
    {
        if (is_a($newPiece, 'Pawn') || is_a($newPiece, 'King')) {
            throw new Exceptions\MoveException('You must choose one of
                Rook, Horsey, Bishop, Queen');
        }
    }
}
