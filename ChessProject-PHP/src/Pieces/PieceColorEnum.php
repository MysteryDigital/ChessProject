<?php

namespace Pieces;

class PieceColorEnum
{
    const WHITE = 1;
    const BLACK = 2;

    public static function getColor($id)
    {
        return $id == self::WHITE ? 'White' : 'Black';
    }
}
