<?php

/**
 * PieceFactory.php.
 *
 * PHP version 5.4+
 *
 * @author     Andy Beak <andy.beak@protonmail.ch>
 * @copyright  2015 Logic Now
 * @license    Commercial - All rights reserved
 *
 * @version    1.0.0
 *
 * @since      File available since Release 1.0.0
 */

namespace Pieces;

use Exception;

/**
 * Implement a Factory to provide pieces.
 *
 * @author      Andy Beak <andy.beak@protonmail.ch>
 * @copyright   2015 Logic Now
 * @license     Commercial
 *
 * @since       Class available since Release 1.0.0
 */
class PieceFactory
{
    public static function make($pieceType, $colour)
    {
        $piece = 'Pieces\\'.ucfirst($pieceType);

        $colour = (int) $colour;

        if ($colour !== PieceColorEnum::WHITE && $colour !== PieceColorEnum::BLACK) {
            throw new Exception('Invalid piece colour');
        }

        if (class_exists($piece)) {
            return new $piece($colour);
        } else {
            throw new Exception($piece.' has not been implemented');
        }
    }
}
