<?php

/**
 * Player.
 *
 * PHP version 5.4+
 *
 * @author     Andy Beak <andy.beak@double-eye.com>
 * @copyright  2015 Logic Now
 * @license    Commercial - All rights reserved
 *
 * @version    1.0.0
 *
 * @since      File available since Release 1.0.0
 */

namespace LogicNow;


class Player
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int;
     */
    private $pieceColor;

    public function __construct($name, $colour)
    {
        $this->name = filter_var($name, FILTER_SANITIZE_STRING);  // prevent xss
        $this->colour = $colour;
    }

    public function getColour()
    {
    	return $this->pieceColor;
    }

    public function getName()
    {
    	return $this->name;
    }
}
