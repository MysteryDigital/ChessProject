<?php

/**
 * CastleTest.php.
 *
 * PHP version 5.4+
 *
 * @author     Andy Beak <andy.beak@protonmail.ch>
 * @copyright  2015 Logic Now
 * @license    Commercial - All rights reserved
 *
 * @version    1.0.0
 *
 * @since      File available since Release 1.0.0
 */

namespace LogicNow\Test;

use Exceptions\PositionException;
use LogicNow\ChessBoard;
use Moves\MovementTypeEnum;
use Pieces\Castle;
use Pieces\PieceColorEnum;
use Pieces\PieceFactory;

/**
 * CastleTest class - Defines tests for a castle implementation.
 * Note: The move "castling" is to be implemented as a king move as moving the
 * king two spaces is a distinguishing feature of that move, but it is possible
 * to move the castle two spaces without intending to castle.
 *
 * @author      Andy Beak <andy.beak@protonmail.ch>
 * @copyright   2015 Logic Now
 * @license     Commercial
 *
 * @since       Class available since Release 1.0.0
 */
class CastleTest extends \PHPUnit_Framework_TestCase
{

    /** @var  ChessBoard */
    private $chessBoard;
    /** @var  Pawn */
    private $testSubject;

    protected function setUp()
    {
        $this->chessBoard = new ChessBoard();

        $this->testSubject = PieceFactory::make('castle', PieceColorEnum::WHITE);

        $this->testSubject->setChessboard($this->chessBoard);
    }

    /**
     * @dataProvider Diagonal_from_4_4_Provider
     */
    public function testCastle_Move_IllegalCoordinates_Diagonal_DoesNotMove($x, $y)
    {
        $this->chessBoard->addPiece($this->testSubject, 4, 4);

        $this->testSubject->move(MovementTypeEnum::MOVE(), $x, $y);

        $this->assertEquals(4, $this->testSubject->getXCoordinate());

        $this->assertEquals(4, $this->testSubject->getYCoordinate());
    }

    public function testCastle_Move_IllegalCoordinates_Off_Board_DoesNotMove()
    {
    	$max = ChessBoard::MAX_BOARD_WIDTH;

    	$offBoard = $max + 1;

        $this->chessBoard->addPiece($this->testSubject, 1, 1);

        $this->testSubject->move(MovementTypeEnum::MOVE(), $offBoard, 3);

        $this->assertEquals(6, $this->testSubject->getXCoordinate());

        $this->assertEquals(3, $this->testSubject->getYCoordinate());
    }

    public function testCastle_Move_LegalCoordinates_Forward_UpdatesCoordinates()
    {
    	$minY = ChessBoard::MIN_BOARD_HEIGHT;

    	$maxY = ChessBoard::MAX_BOARD_HEIGHT;

        $this->chessBoard->addPiece($this->testSubject, 1, $minY);

        $this->testSubject->move(MovementTypeEnum::MOVE(), 1, $maxY);

        $this->assertEquals(1, $this->testSubject->getXCoordinate());

        $this->assertEquals($maxY, $this->testSubject->getYCoordinate());
    }

    public function testCastle_Move_LegalCoordinates_Backwards_UpdatesCoordinates()
    {
    	$minY = ChessBoard::MIN_BOARD_HEIGHT;

    	$maxY = ChessBoard::MAX_BOARD_HEIGHT;

        $this->chessBoard->addPiece($this->testSubject, 1, $maxY);

        $this->testSubject->move(MovementTypeEnum::MOVE(), 1, $minY);

        $this->assertEquals(1, $this->testSubject->getXCoordinate());

        $this->assertEquals($minY, $this->testSubject->getYCoordinate());
    }


    public function testCastle_Move_LegalCoordinates_Right_UpdatesCoordinates()
    {
    	$minX = ChessBoard::MIN_BOARD_WIDTH;

    	$maxX = ChessBoard::MAX_BOARD_WIDTH;

        $this->chessBoard->addPiece($this->testSubject, $minX, 1);

        $this->testSubject->move(MovementTypeEnum::MOVE(), $maxX, 1);

        $this->assertEquals($maxX, $this->testSubject->getXCoordinate());

        $this->assertEquals(1, $this->testSubject->getYCoordinate());
    }

    public function testCastle_Move_LegalCoordinates_Left_UpdatesCoordinates()
    {
    	$minX = ChessBoard::MIN_BOARD_WIDTH;

    	$maxX = ChessBoard::MAX_BOARD_WIDTH;

        $this->chessBoard->addPiece($this->testSubject, $maxX, 1);

        $this->testSubject->move(MovementTypeEnum::MOVE(), $minX, 1);

        $this->assertEquals($minX, $this->testSubject->getXCoordinate());

        $this->assertEquals(1, $this->testSubject->getYCoordinate());
    }

    public function testCastle_Move_Illegal_Through_Another_piece()
    {
    	$minX = ChessBoard::MIN_BOARD_WIDTH;

    	$maxX = ChessBoard::MAX_BOARD_WIDTH;

        $this->chessBoard->addPiece($this->testSubject, $minX, 1);

        $pawn = PieceFactory::make('pawn', PieceColorEnum::WHITE);

        $this->chessBoard->addPiece($pawn, $minX + 2, 1);	// two to the right of the castle

        $this->testSubject->move(MovementTypeEnum::MOVE(), $maxX, 1);

        $this->assertEquals($minX, $this->testSubject->getXCoordinate());

        $this->assertEquals(1, $this->testSubject->getYCoordinate());
    }

    public function testCastle_Move_Legal_Capture_Another_piece()
    {
    	$minX = ChessBoard::MIN_BOARD_WIDTH;

    	$maxX = ChessBoard::MAX_BOARD_WIDTH;

        $this->chessBoard->addPiece($this->testSubject, $minX, 1);

        $pawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);

        $this->chessBoard->addPiece($pawn, $maxX, 1);

        $this->testSubject->move(MovementTypeEnum::MOVE(), $maxX, 1);

        $this->assertEquals($maxX, $this->testSubject->getXCoordinate());

        $this->assertEquals(1, $this->testSubject->getYCoordinate());
    }

    public function Diagonal_from_4_4_Provider()
    {
        return [
        	[5, 3],	 // up left
        	[5, 5],	 // up right
        	[3, 3],  // down left
        	[3, 5]   // down right
        ];
    }

}