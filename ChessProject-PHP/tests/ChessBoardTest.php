<?php

namespace LogicNow\Test;


use LogicNow\ChessBoard;
use Moves\MovementTypeEnum;
use Pieces\PieceColorEnum;
use Pieces\Pawn;
use Pieces\PieceFactory;

class ChessBoardTest extends \PHPUnit_Framework_TestCase
{

    /** @var  ChessBoard */
    private $_testSubject;

    public function setUp()
    {
        $this->_testSubject = new ChessBoard();
    }

    public function testHas_MaxBoardWidth_of_8()
    {
        $this->assertEquals(8, ChessBoard::MAX_BOARD_WIDTH);
    }

    public function testHas_MaxBoardHeight_of_7()
    {
        $this->assertEquals(8, ChessBoard::MAX_BOARD_HEIGHT);
    }

    public function testIsLegalBoardPosition_False_X_equals_0_Y_equals_0()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(0, 0);
        $this->assertFalse($isValidPosition);
    }

    public function testIsLegalBoardPosition_False_X_equals_min_Y_equals_min()
    {
        $minX = ChessBoard::MIN_BOARD_WIDTH;
        $minY = ChessBoard::MIN_BOARD_HEIGHT;
        $isValidPosition = $this->_testSubject->isLegalBoardPosition($minX, $minY);
        $this->assertTrue($isValidPosition);
    }

    public function testIsLegalBoardPosition_True_X_equals_5_Y_equals_5()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(5, 5);
        $this->assertTrue($isValidPosition);
    }

    public function testIsLegalBoardPosition_False_X_equals_11_Y_equals_5()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(11, 5);
        $this->assertFalse($isValidPosition);
    }

    public function testIsLegalBoardPosition_False_X_equals_1_Y_equals_more_than_max()
    {
        $maxY = ChessBoard::MAX_BOARD_HEIGHT;
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(1, $maxY + 1);
        $this->assertFalse($isValidPosition);
    }

    public function testIsLegalBoardPosition_False_X_equals_a_Y_equals_2()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition('a', 2);
        $this->assertFalse($isValidPosition);
    }

    public function testIsLegalBoardPosition_False_X_equals_float_Y_equals_2()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition('1.5', 2);
        $this->assertFalse($isValidPosition);
    }

    public function testIsLegalBoardPosition_False_X_equals_11_Y_equals_1()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(11, 1);
        $this->assertFalse($isValidPosition);
    }

    public function testIsLegalBoardPosition_False_For_Negative_Y_Values()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(5, -1);
        $this->assertFalse($isValidPosition);
    }

    public function testAvoids_Duplicate_Positioning()
    {
        $firstPawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);
        $secondPawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);
        $this->_testSubject->addPiece($firstPawn, 6, 3);
        $this->_testSubject->addPiece($secondPawn, 6, 3);
        $this->assertEquals(6, $firstPawn->getXCoordinate());
        $this->assertEquals(3, $firstPawn->getYCoordinate());
        $this->assertEquals(-1, $secondPawn->getXCoordinate());
        $this->assertEquals(-1, $secondPawn->getYCoordinate());
    }

    /**
     * Refactored this test to make it more clear what we are doing
     * and to avoid having non-integer row values (which should be tested
     * seperately).  We loop across a rank simply to avoid duplicate positions.
     */
    public function testLimits_The_Number_Of_Pawns()
    {
        $minX = ChessBoard::MIN_BOARD_WIDTH;
        $maxX = ChessBoard::MAX_BOARD_WIDTH;
        $invalidX = $maxX + 2;

        for ($x = $minX; $x <= $maxX; $x++) {

            $pawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);

            $y = 2;

            $maxAllowed = $pawn->getMaxAllowed();

            $this->_testSubject->addPiece($pawn, $x, $y);

            $numberOfPawns = $x + 1;

            if ($numberOfPawns <= $maxAllowed) {
                $this->assertEquals($x, $pawn->getXCoordinate());
                $this->assertEquals($y, $pawn->getYCoordinate());
            } else {
                $this->assertEquals(-1, $pawn->getXCoordinate());
                $this->assertEquals(-1, $pawn->getYCoordinate());
            }

        }
    }

    public function testGet_piece_at_returns_the_same_piece()
    {
        $pawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);

        $pawn->dummyProperty = 'Test';

        $this->_testSubject->addPiece($pawn, 6, 2);

        $returned = $this->_testSubject->getPieceAt(6,2);

        $this->assertSame($pawn->dummyProperty, $returned->dummyProperty);
    }

    public function testGet_piece_at_returns_zero_for_empty_square()
    {
        $returned = $this->_testSubject->getPieceAt(6,2);

        $this->assertSame(0, $returned);
    }

    public function testMove_to_position_of_opposing_color_piece_is_capture()
    {
        $firstPawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);
        $secondPawn = PieceFactory::make('pawn', PieceColorEnum::WHITE);
        $this->_testSubject->addPiece($firstPawn, 6, 3);
        // TODO: correct when we implement capture movement type validity checking
        //      in tdd we would probably correct the test and let it fail until the code works
        $this->_testSubject->addPiece($secondPawn, 6, 2);

        $movementType = $this->_testSubject->getMoveType(6,3,6,2);

        $this->assertEquals($movementType, MovementTypeEnum::CAPTURE());
    }

        public function testMove_to_position_of_empty_square_is_move()
    {
        $firstPawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);
        $this->_testSubject->addPiece($firstPawn, 6, 3);

        $movementType = $this->_testSubject->getMoveType(6,3,6,2);

        $this->assertEquals($movementType, MovementTypeEnum::MOVE());
    }

    /**
     * @expectedException \Exceptions\MoveException
     */
    public function testCannot_move_to_position_of_same_colour_piece()
    {
        $firstPawn = PieceFactory::make('pawn', PieceColorEnum::WHITE);
        $secondPawn = PieceFactory::make('pawn', PieceColorEnum::WHITE);
        $this->_testSubject->addPiece($firstPawn, 6, 3);
        $this->_testSubject->addPiece($secondPawn, 6, 2);   // TODO: correct when we implement capture movement type validity checking

        $movementType = $this->_testSubject->getMoveType(6,3,6,2);

    }

    public function testGetStandardNotation_for_bottom_left_is_a1()
    {
    	$minX = ChessBoard::MIN_BOARD_WIDTH;
        $minY = ChessBoard::MIN_BOARD_HEIGHT;

    	$square = $this->_testSubject->getStandardNotation($minX, $minY);
    }

}