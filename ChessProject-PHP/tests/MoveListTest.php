<?php
/**
 * MoveListTest.php.
 *
 * PHP version 5.4+
 *
 * @author     Andy Beak <andy.beak@protonmail.ch>
 * @copyright  2015 Logic Now
 * @license    Commercial - All rights reserved
 *
 * @version    1.0.0
 *
 * @since      File available since Release 1.0.0
 */

namespace LogicNow\Test;


use Moves\MoveList;
use Moves\MoveListItem;
use Moves\MovementTypeEnum;
use LogicNow\ChessBoard;
use Pieces\PieceColorEnum;
use Pieces\Pawn;
use Pieces\PieceFactory;


class MoveListTest extends \PHPUnit_Framework_TestCase
{

    /**
     * PSR2: Property names SHOULD NOT be prefixed with a single underscore to
     * indicate protected or private visibility.
     *
     * @var  testSubject
     * @type MoveList
     */
    private $testSubject;

    public function setUp()
    {
        $this->testSubject = new MoveList();
    }

    public function testAddMove_Updates_MoveList()
    {
    	$before = $this->testSubject->getMoveList();

    	$move = MovementTypeEnum::MOVE();

        $chessBoard = new ChessBoard();

        $piece = PieceFactory::make('pawn', PieceColorEnum::WHITE);

        $chessBoard->addPiece($piece, 6, 3);

    	$this->testSubject->addMove(
    		$move,
    		$piece,
    		6,
    		4,
    		1
		);

		$after = $this->testSubject->getMoveList();

		$this->assertNotEquals($before, $after);

    }

    public function testAddMove_Increments_move_counter()
    {
    	$move = MovementTypeEnum::MOVE();

        $chessBoard = new ChessBoard();

        $piece = PieceFactory::make('pawn', PieceColorEnum::WHITE);

        $chessBoard->addPiece($piece, 6, 3);

    	$this->testSubject->addMove(
    		$move,
    		$piece,
    		6,
    		4,
    		1
		);

		$this->assertSame(1, $this->testSubject->getMoveNum());
    }

    public function testGetMove_Returns_the_move()
    {

    	$move = MovementTypeEnum::MOVE();

        $chessBoard = new ChessBoard();

        $piece = PieceFactory::make('pawn', PieceColorEnum::WHITE);

        $chessBoard->addPiece($piece, 6, 3);

    	$this->testSubject->addMove(
    		$move,
    		$piece,
    		6,
    		4,
    		1
		);

    	$historyMove = $this->testSubject->getMove(1);

    	$this->assertEquals(6, $historyMove->getFromX());
    	$this->assertEquals(6, $historyMove->getToX());
    	$this->assertEquals(3, $historyMove->getFromY());
    	$this->assertEquals(4, $historyMove->getToY());
    }

    public function testMoveNotation()
    {
    	$move = MovementTypeEnum::MOVE();

        $chessBoard = new ChessBoard();

        $piece = PieceFactory::make('pawn', PieceColorEnum::WHITE);

        $chessBoard->addPiece($piece, ChessBoard::MIN_BOARD_WIDTH, 2);

    	$this->testSubject->addMove(
    		$move,
    		$piece,
    		ChessBoard::MIN_BOARD_WIDTH,
    		4,
    		1
		);

    	$notation = $this->testSubject->getMoveNotation(1);

    	$this->assertEquals('Pa2-a4', $notation);
    }
}