<?php

namespace LogicNow\Test;

use Exceptions\PositionException;
use LogicNow\ChessBoard;
use Moves\MovementTypeEnum;
use Pieces\Pawn;
use Pieces\PieceColorEnum;
use Pieces\PieceFactory;


class PawnTest extends \PHPUnit_Framework_TestCase
{

    /** @var  ChessBoard */
    private $_chessBoard;
    /** @var  Pawn */
    private $_testSubject;

    protected function setUp()
    {
        $this->_chessBoard = new ChessBoard();
        $this->_testSubject = PieceFactory::make('pawn', PieceColorEnum::WHITE);
        $this->_testSubject->setChessboard($this->_chessBoard);
    }

    public function testChessBoard_Add_Sets_XCoordinate()
    {
        $this->_chessBoard->addPiece($this->_testSubject, 6, 3);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
    }

    public function testChessBoard_Add_Sets_YCoordinate()
    {
        $this->_chessBoard->addPiece($this->_testSubject, 6, 3);
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawn_Move_IllegalCoordinates_Right_DoesNotMove()
    {
        $this->_chessBoard->addPiece($this->_testSubject, 6, 3);
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 7, 3);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawn_Move_IllegalCoordinates_Left_DoesNotMove()
    {
        $this->_chessBoard->addPiece($this->_testSubject, 6, 3);
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 4, 3);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawn_Move_LegalCoordinates_Forward_UpdatesCoordinates()
    {
        $this->_chessBoard->addPiece($this->_testSubject, 6, 3);
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 6, 4);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(4, $this->_testSubject->getYCoordinate());
    }

    public function testPawn_Move_IllegalCoordinates_Backward_DoesNotMove()
    {
        $this->_chessBoard->addPiece($this->_testSubject, 6, 3);
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 6, 2);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawn_Move_IllegalCoordinates_Too_Far_DoesNotMove()
    {
        $this->_chessBoard->addPiece($this->_testSubject, 6, 3);
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 6, 6);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawn_Move_LegalCoordinates_Two_spaces_first_move_UpdatesCoordinates()
    {
        $this->_chessBoard->addPiece($this->_testSubject, 6, 3);
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 6, 5);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(5, $this->_testSubject->getYCoordinate());
    }

    public function testPawn_Move_LegalCoordinates_Two_spaces_second_move_DoesNotMove()
    {
        $this->_chessBoard->addPiece($this->_testSubject, 6, 3);

        $this->_testSubject->move(MovementTypeEnum::MOVE(), 6, 5);

        $this->_testSubject->move(MovementTypeEnum::MOVE(), 6, 7);

        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(5, $this->_testSubject->getYCoordinate());
    }

    /**
     * @dataProvider Valid_diagonal_capture_points_from_4_2_White_Provider
     */
    public function testPawn_Valid_Capture_Updates_Coordinates_White($x, $y)
    {
    	$this->_chessBoard->addPiece($this->_testSubject, 4, 2);

    	$blackPawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);

		$this->_chessBoard->addPiece($blackPawn, $x, $y);

		$this->_testSubject->move(MovementTypeEnum::CAPTURE(), $x, $y);

		$this->assertEquals($x, $this->_testSubject->getXCoordinate());

        $this->assertEquals($y, $this->_testSubject->getYCoordinate());

    }

    public function Valid_diagonal_capture_points_from_4_2_White_Provider()
    {
        return [
        	[3, 3],	 // up left
        	[5, 3]	 // up right
        ];
    }

    /**
     * @dataProvider Valid_diagonal_capture_points_from_4_7_Black_Provider
     */
    public function testPawn_Valid_Capture_Updates_Coordinates_Black($x, $y)
    {
    	$blackPawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);

		$this->_chessBoard->addPiece($blackPawn, 4, 7);

    	$this->_chessBoard->addPiece($this->_testSubject, $x, $y);

		$this->_testSubject->move(MovementTypeEnum::CAPTURE(), $x, $y);

		$this->assertEquals($x, $this->_testSubject->getXCoordinate());

        $this->assertEquals($y, $this->_testSubject->getYCoordinate());

    }

    public function Valid_diagonal_capture_points_from_4_7_Black_Provider()
    {
        return [
        	[3, 6],	 // down left
        	[5, 6]	 // down right
        ];
    }


    /**
     * @dataProvider Invalid_capture_points_from_4_2_White_Provider
     */
    public function testPawn_Invalid_Capture_Updates_Coordinates_White($x, $y)
    {
    	$this->_chessBoard->addPiece($this->_testSubject, 4, 2);

    	$blackPawn = PieceFactory::make('pawn', PieceColorEnum::BLACK);

		$this->_chessBoard->addPiece($blackPawn, $x, $y);

		$this->_testSubject->move(MovementTypeEnum::CAPTURE(), $x, $y);

		$this->assertEquals(4, $this->_testSubject->getXCoordinate());

        $this->assertEquals(2, $this->_testSubject->getYCoordinate());

    }

    public function Invalid_capture_points_from_4_2_White_Provider()
    {
        return [
        	[4, 2],	 // straight ahead
        	[4, 1],	 // backwards
        	[3, 1],	 // backwards left
        	[3, 1],	 // backwards right
        	[3, 2],	 // left
        	[5, 2],  // right
        	[2, 4],	 // two diagonal left
        ];
    }

}